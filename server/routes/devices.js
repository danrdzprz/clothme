import routerx from 'express-promise-router';
import deviceController from '../controllers/DeviceController';
import auth from '../middlewares/auth'
const router = routerx();

/** POST /api/device */
router.post('/',auth.verifyUser,deviceController.store);
/** GET /api/device */
router.get('/',auth.verifyUser,deviceController.index);
/** PUT /api/device */
router.put('/',auth.verifyUser,deviceController.update);
/** DELETE /api/device */
router.delete('/',auth.verifyUser,deviceController.remove);
/** DELETE /api/device/activate */
router.put('/activate',auth.verifyUser,deviceController.activate);
/** PUT /api/device/deactivate */
router.put('/deactivate',auth.verifyUser,deviceController.deactivate);
/** GET /api/user/:_id */
router.get('/:_id',auth.verifyUser,deviceController.show);
/** POST /api/device/assign */
router.post('/assign',auth.verifyUser,deviceController.assign);

export default router;