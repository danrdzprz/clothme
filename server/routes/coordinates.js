import routerx from 'express-promise-router';
import coordinateController from '../controllers/CoordinateController';
import auth from '../middlewares/auth'
const router = routerx();

/** GET /api/coordinate/events */
router.get('/test',coordinateController.test);
/** GET /api/coordinate/events */
router.get('/events',auth.verifyUser,coordinateController.events);
/** POST /api/coordinate */
router.post('/',coordinateController.store);
/** GET /api/coordinate */
router.get('/',auth.verifyUser,coordinateController.index);
/** PUT /api/coordinate */
router.put('/',auth.verifyUser,coordinateController.update);
/** DELETE /api/coordinate */
router.delete('/',auth.verifyUser,coordinateController.remove);
/** DELETE /api/coordinate/activate */
router.put('/activate',auth.verifyUser,coordinateController.activate);
/** PUT /api/coordinate/deactivate */
router.put('/deactivate',auth.verifyUser,coordinateController.deactivate);
/** GET /api/user/:_id */
router.get('/:_id',auth.verifyUser,coordinateController.show);
/** POST /api/coordinate/assign */
router.post('/assign',auth.verifyUser,coordinateController.assign);

export default router;