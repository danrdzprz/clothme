import routerx from 'express-promise-router';
import userController from '../controllers/UserController';
import auth from '../middlewares/auth'
const router = routerx();

/** POST /api/user */
router.post('/',auth.verifyUser,userController.store);
/** GET /api/user */
router.get('/',auth.verifyUser,userController.index);
/** PUT /api/user */
router.put('/',auth.verifyUser,userController.update);
/** DELETE /api/user */
router.delete('/',auth.verifyUser,userController.remove);
/** GET /api/user/me */
router.get('/me',auth.verifyUser,userController.me);
/** DELETE /api/user/activate */
router.put('/activate',auth.verifyUser,userController.activate);
/** PUT /api/user/deactivate */
router.put('/deactivate',auth.verifyUser,userController.deactivate);
/** PUT /api/user/login */
router.post('/login',userController.login);
/** GET /api/user/logout */
router.post('/logout',auth.verifyUser,userController.logout);
/** GET /api/user/:_id */
router.get('/:_id',auth.verifyUser,userController.show);

export default router;