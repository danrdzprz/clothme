import routerx from 'express-promise-router';
import userRouter from './user';
import devicesRouter from './devices';
import coordinatesRouter from "./coordinates";
/**
 *  Provides all routes
 * 
 * @return object
 */
const router = routerx();

/** ROUTES PREFIX /api/user */
router.use('/user',userRouter);

/** ROUTES PREFIX /api/devices */
router.use('/devices',devicesRouter);


/** ROUTES PREFIX /api/coordinates */
router.use('/coordinates',coordinatesRouter);

export default router;