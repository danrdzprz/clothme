import moongose,{Schema} from 'mongoose';

const deviceSchema = new Schema({
    name: {type: String, maxlength: 50},
    password: {type: String, maxlength:64, required: true },
    // mac: {type: String, maxlength:20,default:null},
    user: {type: Schema.ObjectId, ref:'user'},
    description: {type: String, maxlength:256},
    status: {type: Number, default: 1},
    deleted_at: {type: Number, default: null},
    created_at: {type: Date, default: Date.now}
});

const Device = moongose.model('device',deviceSchema,'devices');
export default Device;