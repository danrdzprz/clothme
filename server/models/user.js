import mongoose, {Schema} from 'mongoose';

const userSchema = new Schema({
    username: { type:String, maxlength:50, unique:true, required:true},
    name: { type:String,maxlength:50, required:true},
    last_name: { type:String,maxlength:50, required:true},
    email: { type:String, maxlength:50, unique:true},
    password: { type:String, maxlength:64, required:true},
    created_at: { type: Date, default: Date.now },
	deleted_at: { type: Date, default: null }
});

const User = mongoose.model('user',userSchema,'users');
export default User;

