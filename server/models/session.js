import moongose,{Schema} from 'mongoose';

const sessionSchema = new Schema({
    user: {type:Schema.ObjectId,ref:'user', required:true},
    remember_token: { type:Number, default:0},
    user_agent : {type:String,maxlength:254,required:true},
    token : {type:String,maxlength:254,required:true},
});

const Session = moongose.model('session',sessionSchema,'sessions');
export default Session;