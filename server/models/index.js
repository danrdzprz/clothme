import User from './user';
import Session from './session';
import Device from './device';
import Coordinate from "./coordinate";
import Blacklist from "./blacklist";

/**
 * Provides all database models
 * 
 * @return object
 */
export default {
    User,
    Session,
    Device,
    Coordinate,
    Blacklist
};