import moongose,{Schema} from 'mongoose';

const blacklistSchema = new Schema({
    token : {type:String,required:true},
    created_at: {type: Date, default: Date.now}
});

const Blacklist = moongose.model('blacklist',blacklistSchema,'blacklists');
export default Blacklist;