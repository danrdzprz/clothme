import moongose,{Schema} from 'mongoose';

const coordinateSchema = new Schema({
    device: {type:Schema.ObjectId,ref:'device', required:true},
    latitude : {type: Number, required: true },
    longitude: {type: Number, required: true},
    created_at: {type: Date, default: Date.now}
});
const Coordinate = moongose.model('coordinate',coordinateSchema,'coordinates');
export default Coordinate;