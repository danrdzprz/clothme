import models from '../models';
import brcypt from 'bcryptjs';
import token from '../services/token';
// import redis_client from '../providers/redis';

export default {
    /**
     * Authenticate for user
     * @params
     * email {string}
     * password {string}
    */ 
    login: async(req,res,next)=>{
        try {
          let user = await models.User.findOne({email:req.body.email,deleted_at:null});
          if(user){
              //existe un User con ese email
              let match = await brcypt.compare(req.body.password,user.password);
              if(match){
                  let tokenReturn = await token.encode(user._id,user.email);
                  const session = await models.Session.create({user:user._id,token:tokenReturn,user_agent:req.headers['user-agent']});
                  res.status(200).json({user,tokenReturn});
                  /** Set a cookie */
                  // res.status(200)
                  // .cookie('token', tokenReturn, { maxAge: 86400 })
                  // .json({user});
              }
              else{
                  res.status(404).send({
                      message: 'Password incorrecto'
                  });
              }
          }
          else{
            res.status(404).send({
                message: 'No existe el Usuario'
            });
          }
        } catch (e) {
            res.status(500).send({
                message: 'Ocurrió un error' 
            });
            next(e);
        }
    },
    /**
     * Show current user
    */ 
    me : async(req,res,next) => {
        try {
            const response = await token.decode(req.headers.token);
            res.status(200).json({response});
        } catch (e) {
            res.status(500).send({
                message: 'Ocurrió un error' 
            });
            next(e);
        }
    },
    /**
     * Logout current user and add his own token in a blacklist
    */ 
    logout : async(req,res,next) => {
        try {
            const response = await token.decode(req.headers.token);
            const reg = await models.Session.findOneAndDelete({user:response._id,token:req.headers.token},{useFindAndModify: false});
            // redis_client.SADD('token_blacklist',req.headers.token,(err,res)=>{
            // });
            const token_blacklist = await models.Blacklist.create({token:req.headers.token});
            res.status(200).json({response});
        } catch (e) {
            res.status(500).send({
                message: 'Ocurrió un error' 
            });
            next(e);
        }
    }
}