import models from '../models';
import brcypt from 'bcryptjs';
import token from '../services/token';

export default {
    test: async(req,res,next) => {
        // console.log(req.query);
        try {
          const io = req.app.get('io');
          //get start and end coordinates of current day
          var start = new Date();
          start.setHours(0,0,0,0);
          var end = new Date();
          end.setHours(23,59,59,999);
          //
          const past_coord = await models.Coordinate.findOne(
          {
            device: req.query.device,
            created_at: {
                $gte: start,
                $lt: end
            }
          })
          .sort({'created_at':-1})
          .limit(1);
          let reg = await models.Coordinate.create(req.query);
          reg = await reg.populate('device',{name:1}).execPopulate();
          const device = await models.Device.findOne({_id:req.query.device},{name:1,user:1});

          if(device.user){
            reg.device = device;
            io.in('coordinates:'+device.user).emit('change_marker', reg);
            if(past_coord){
              const new_coords={
                _old:past_coord,
                _new:reg
              }
              io.in('coordinates:'+device.user).emit('new_line', new_coords);
            }
          }

          res.status(200).json(reg);
        } catch (e) {
          res.status(500).send({
            message: 'Ocurrió un error' 
          });
          next(e);
        }
      
    },
    /**
     * Coordinate Register
     * @body
     * device {string}
     * latitude {string}         
     * longitude {string}
    */ 
    store: async(req,res,next) => {
        try {
          const io = req.app.get('io');
          //get start and end coordinates of current day
          var start = new Date();
          start.setHours(0,0,0,0);
          var end = new Date();
          end.setHours(23,59,59,999);
          //
          const past_coord = await models.Coordinate.findOne(
          {
            device: req.body.device,
            created_at: {
                $gte: start,
                $lt: end
            }
          })
          .sort({'created_at':-1})
          .limit(1);
          let reg = await models.Coordinate.create(req.body);
          reg = await reg.populate('device',{name:1}).execPopulate();
          const device = await models.Device.findOne({_id:req.body.device},{name:1,user:1});

          if(device.user){
            reg.device = device;
            io.in('coordinates:'+device.user).emit('change_marker', reg);
            if(past_coord){
              const new_coords={
                _old:past_coord,
                _new:reg
              }
              io.in('coordinates:'+device.user).emit('new_line', new_coords);
            }
          }

          res.status(200).json(reg);
        } catch (e) {
          res.status(500).send({
            message: 'Ocurrió un error' 
          });
          next(e);
        }
    },
    /**
     * Display an spececific Coordinate
     * @params
     * _id {string}
    */ 
    show:async(req,res,next) => {
        try {
            const reg = await models.Coordinate.findOne({_id:req.params._id});
            if(!reg){
                res.status(404).send({
                    message: 'El registro no existe' 
                });
            }
            else{
                res.status(200).json(reg);
            }
        } catch (e) {
            res.status(500).send({
                message: 'Ocurrió un error' 
            });
            next(e);
        }
    },
    /**
     * Display a list of Coordinates
     * @params optional
     * value {string}
    */ 
    index:async(req,res,next) => {
        try {
            const user = await token.decode(req.headers.token);
            const _devices = await models.Device.find({"user":user._id},{_id:1});
            const devices = _devices.map(device=> {
                return device._id;
            });
            const reg = await models.Coordinate.find({device: { $in: devices}})
                .populate('device',{name:1})
                .sort({'created_at':-1});
            res.status(200).json(reg);
        } catch (e) {
            res.status(500).send({
                message: 'Ocurrió un error' 
            });
            next(e);
        }
    },
    /**
     * Display a list of Coordinates
     * @params optional
     * value {string}
    */ 
    events:async(req,res,next) => {
        try {
            const user = await token.decode(req.headers.token);
            const _devices = await models.Device.find({"user":user._id},{_id:1});
            const devices = _devices.map(device=> {
                return device._id;
            });
            var start = new Date();
            start.setHours(0,0,0,0);
            
            var end = new Date();
            end.setHours(23,59,59,999);
            // const reg = await models.Coordinate.find(
            //     {
            //       device: { $in: devices},
            //       created_at: {
            //           $gte: start,
            //           $lt: end
            //       }
            //     })
            //     .populate('device',{name:1})
            //     .sort({'created_at':-1});
          const reg = await models.Coordinate.aggregate([
              {
                $match: { device: { $in: devices},
                      created_at: {
                          $gte: start,
                          $lt: end
                      }}
              },
              {
                $group : {_id : "$device", coordinates: { $push: "$$ROOT" } }
              },
              {
                $lookup: {
                    from: "devices",
                    localField: "_id",
                    foreignField: "_id",
                    as: "device"
                }
              },
              {
                $sort : {'created_at':-1}
              }
             ]);
            res.status(200).json(reg);
        } catch (e) {
            res.status(500).send({
                message: 'Ocurrió un error' 
            });
            next(e);
        }
    },
    /**
     * Update an sepecific Coordinate
     * @body 
     * _id {string}
     * name {string}
     * last_name {string}         
     * email {string}
     * {password}
    */ 
    update : async(req,res,next) => {
        try {
          const user = await token.decode(req.headers.token);
          let data = {
            name: req.body.name,
            description: req.body.description
          }
          const reg = await models.Coordinate.findOneAndUpdate({_id:req.body._id,user:user._id},{ $set: data },{useFindAndModify: false});
          res.status(200).json(reg);
        } catch (e) {
            res.status(500).send({
                message: 'Ocurrió un error' 
            });
            next(e);
        }
    },
    /**
     * Delete an sepecific Coordinate
     * @body
     * _id {string}
    */ 
    remove:async(req,res,next) => {
        try {
          const user = await token.decode(req.headers.token);
          let data = {
            user: null
          }
          const reg = await models.Coordinate.findOneAndUpdate({_id:req.body._id,user:user._id},{ $set: data },{useFindAndModify: false});
          res.status(200).json(reg);
        } catch (e) {
            res.status(500).send({
                message: 'Ocurrió un error' 
            });
            next(e);
        }
    },
    /**
     * Activate an specific Coordinate
     * @params
     * _id {string}
    */ 
    activate:async(req,res,next) => {
        try {
            const reg = await models.Coordinate.findOneAndUpdate({_id:req.body._id},{deleted_at:null},{useFindAndModify: false});
            res.status(200).json(reg);
        } catch (e) {
            res.status(500).send({
                message: 'Ocurrió un error' 
            });
            next(e);
        }
    },
    /**
     * Deactivate an specific Coordinate
     * @params
     * _id {string}
    */ 
    deactivate:async(req,res,next) => {
        try {
            const reg = await models.Coordinate.findByIdAndUpdate({_id:req.body._id},{deleted_at:Date.now()},{useFindAndModify: false});
            res.status(200).json(reg);
        } catch (e) {
            res.status(500).send({
                message: 'Ocurrió un error' 
            });
            next(e);
        }
    },
    /**
     * Assign Coordinate to user
     * @body
     * name {string}
     * last_name {string}         
     * email {string}
     * {password}
    */ 
    assign: async(req,res,next) => {
      try {
          const user = await token.decode(req.headers.token);
          let data = {
            name: req.body.name,
            description: req.body.description,
            user: user._id
          }
          const Coordinate = await models.Coordinate.findOne({_id:req.body._id});
          let match = await brcypt.compare(req.body.password,Coordinate.password);
          if(match){
            const reg = await models.Coordinate.findOneAndUpdate({_id:Coordinate._id},{ $set: data },{useFindAndModify: false});
            res.status(200).json(reg);
          }
          else{
            res.status(404).send({
              message: 'Password incorrecto'
            });
          }
      } catch (e) {
          res.status(500).send({
              message: 'Ocurrió un error' 
          });
          next(e);
      }
    }
}