import models from '../models';
import brcypt from 'bcryptjs';
import token from '../services/token';
// import redis_client from '../providers/redis';

export default {
    /**
     * Account Register
     * @body
     * name {string}
     * last_name {string}         
     * email {string}
     * {password}
    */ 
    store: async(req,res,next) => {
        try {
            req.body.password = await brcypt.hash(req.body.password,10);
            const reg = await models.User.create(req.body);
            res.status(200).json(reg);
        } catch (e) {
            res.status(500).send({
                message: 'Ocurrió un error' 
            });
            next(e);
        }
    },
    /**
     * Display an spececific User
     * @params
     * _id {string}
    */ 
    show:async(req,res,next) => {
        try {
            const reg = await models.User.findOne({_id:req.params._id});
            if(!reg){
                res.status(404).send({
                    message: 'El registro no existe' 
                });
            }
            else{
                res.status(200).json(reg);
            }
        } catch (e) {
            res.status(500).send({
                message: 'Ocurrió un error' 
            });
            next(e);
        }
    },
    /**
     * Display a list of Users
     * @params optional
     * value {string}
    */ 
    index:async(req,res,next) => {
        try {
            let value = req.query.value;
            const reg = await models.User.find({$or:[{'name':new RegExp(value,'i')},{'email':new RegExp(value,'i')}]},{created_at:0})
            .sort({'created_at':-1});
            res.status(200).json(reg);
        } catch (e) {
            res.status(500).send({
                message: 'Ocurrió un error' 
            });
            next(e);
        }
    },
    /**
     * Update an sepecific user
     * @body 
     * _id {string}
     * name {string}
     * last_name {string}         
     * email {string}
     * {password}
    */ 
    update : async(req,res,next) => {
        try {
            if(req.body.password){
                let pass = req.body.password;
                const User = await models.User.findOne({_id:req.body._id}); 
                if(pass!=User.password){
                    req.body.password = await brcypt.hash(req.body.password,10);
                }
            }
            const reg = await models.User.findByIdAndUpdate({_id:req.body._id},{ $set: req.body },{useFindAndModify: false});
            res.status(200).json(reg)
        } catch (e) {
            res.status(500).send({
                message: 'Ocurrió un error' 
            });
            next(e);
        }
    },
    /**
     * Delete an sepecific user
     * @body
     * _id {string}
    */ 
    remove:async(req,res,next) => {
        try {
            const reg = await models.User.findByIdAndDelete({_id:req.body._id});
            res.status(200).json(reg);
        } catch (e) {
            res.status(500).send({
                message: 'Ocurrió un error' 
            });
            next(e);
        }
    },
    /**
     * Activate an specific user
     * @params
     * _id {string}
    */ 
    activate:async(req,res,next) => {
        try {
            const reg = await models.User.findOneAndUpdate({_id:req.body._id},{deleted_at:null},{useFindAndModify: false});
            res.status(200).json(reg);
        } catch (e) {
            res.status(500).send({
                message: 'Ocurrió un error' 
            });
            next(e);
        }
    },
    /**
     * Deactivate an specific user
     * @params
     * _id {string}
    */ 
    deactivate:async(req,res,next) => {
        try {
            const reg = await models.User.findByIdAndUpdate({_id:req.body._id},{deleted_at:Date.now()},{useFindAndModify: false});
            res.status(200).json(reg);
        } catch (e) {
            res.status(500).send({
                message: 'Ocurrió un error' 
            });
            next(e);
        }
    },
    /**
     * Authenticate for user
     * @params
     * email {string}
     * password {string}
    */ 
    login: async(req,res,next)=>{
        try {
          let user = await models.User.findOne({$or:[{'username':new RegExp(req.body.email,'i')},{'email':new RegExp(req.body.email,'i')}],deleted_at:null});
          if(user){
              //existe un User con ese email
              let match = await brcypt.compare(req.body.password,user.password);
              if(match){
                  let tokenReturn = await token.encode(user._id,user.email);
                  const session = await models.Session.create({user:user._id,token:tokenReturn,user_agent:req.headers['user-agent']});
                  res.status(200).json({user,tokenReturn});
                  /** Set a cookie */
                  // res.status(200)
                  // .cookie('token', tokenReturn, { maxAge: 86400 })
                  // .json({user});
              }
              else{
                  res.status(404).send({
                      message: 'Password incorrecto'
                  });
              }
          }
          else{
            res.status(404).send({
                message: 'No existe el Usuario'
            });
          }
        } catch (e) {
            res.status(500).send({
                message: 'Ocurrió un error' 
            });
            next(e);
        }
    },
    /**
     * Show current user
    */ 
    me : async(req,res,next) => {
        try {
            const response = await token.decode(req.headers.token);
            res.status(200).json({response});
        } catch (e) {
            res.status(500).send({
                message: 'Ocurrió un error' 
            });
            next(e);
        }
    },
    /**
     * Logout current user and add his own token in a blacklist
    */ 
    logout : async(req,res,next) => {
        try {
            const response = await token.decode(req.headers.token);
            const session = await models.Session.findOneAndDelete({user:response._id,token:req.headers.token},{useFindAndModify: false});
            const token_blacklist = await models.Blacklist.create({token:req.headers.token});
            // redis_client.SADD('token_blacklist',req.headers.token,(err,res)=>{
            // });
            res.status(200).json({response});
        } catch (e) {
            res.status(500).send({
                message: 'Ocurrió un error' 
            });
            next(e);
        }
    }
}