import models from '../models';
import brcypt from 'bcryptjs';
import token from '../services/token';

async function asyncForEach(array, callback) {
  let data = []
  for (let index = 0; index < array.length; index++) {
    const element = await callback(array[index], index, array);
    data.push( element );
  }
  return data
}

export default {
    /**
     * Device Register
     * @body
     * name {string}
     * last_name {string}         
     * email {string}
     * {password}
    */ 
    store: async(req,res,next) => {
        try {
            req.body.password = await brcypt.hash(req.body.password,10);
            const reg = await models.Device.create(req.body);
            res.status(200).json(reg);
        } catch (e) {
            res.status(500).send({
                message: 'Ocurrió un error' 
            });
            next(e);
        }
    },
    /**
     * Display an spececific Device
     * @params
     * _id {string}
    */ 
    show:async(req,res,next) => {
        try {
            const reg = await models.Device.findOne({_id:req.params._id});
            if(!reg){
                res.status(404).send({
                    message: 'El registro no existe' 
                });
            }
            else{
                res.status(200).json(reg);
            }
        } catch (e) {
            res.status(500).send({
                message: 'Ocurrió un error' 
            });
            next(e);
        }
    },
    /**
     * Display a list of Devices
     * @params optional
     * value {string}
    */ 
    index:async(req,res,next) => {
        try {
            const user = await token.decode(req.headers.token);
            let value = req.query.value;
            // const reg = await models.Device.find({$or:[{'name':new RegExp(value,'i')},{'email':new RegExp(value,'i')}]},{created_at:0})
            const reg = await models.Device.find({"user":user._id},{created_at:0})
            .sort({'created_at':-1});
            //get start and end coordinates of current day
            var start = new Date();
            start.setHours(0,0,0,0);
            
            var end = new Date();
            end.setHours(23,59,59,999);

            const _devices = await asyncForEach(reg, async (device,index) => {
                let coordinates = [];
                const old_coord = await models.Coordinate.findOne(
                  {
                    device: device._id,
                    created_at: {
                        $gte: start,
                        $lt: end
                    }
                  })
                  .sort({'created_at':1})
                  .limit(1);
                if(old_coord){
                  coordinates.push(old_coord);
                }
                const new_coord = await models.Coordinate.findOne(
                    {
                      device: device._id,
                      created_at: {
                          $gte: start,
                          $lt: end
                      }
                    })
                    .sort({'created_at':-1})
                    .limit(1);
                if(new_coord){
                  coordinates.push(new_coord);
                }
                
                return {
                  _id: device._id,
                  name: device.name,
                  mac: device.mac,
                  description: device.description,
                  coordinates:coordinates
                };
              });
            res.status(200).json(_devices);
        } catch (e) {
            res.status(500).send({
                message: 'Ocurrió un error' 
            });
            next(e);
        }
    },
    /**
     * Update an sepecific Device
     * @body 
     * _id {string}
     * name {string}
     * last_name {string}         
     * email {string}
     * {password}
    */ 
    update : async(req,res,next) => {
        try {
          const user = await token.decode(req.headers.token);
          let data = {
            name: req.body.name,
            description: req.body.description
          }
          const reg = await models.Device.findOneAndUpdate({_id:req.body._id,user:user._id},{ $set: data },{useFindAndModify: false});
          res.status(200).json(reg);
        } catch (e) {
            res.status(500).send({
                message: 'Ocurrió un error' 
            });
            next(e);
        }
    },
    /**
     * Delete an sepecific Device
     * @body
     * _id {string}
    */ 
    remove:async(req,res,next) => {
        try {
          const user = await token.decode(req.headers.token);
          let data = {
            user: null
          }
          const reg = await models.Device.findOneAndUpdate({_id:req.body._id,user:user._id},{ $set: data },{useFindAndModify: false});
          res.status(200).json(reg);
        } catch (e) {
            res.status(500).send({
                message: 'Ocurrió un error' 
            });
            next(e);
        }
    },
    /**
     * Activate an specific Device
     * @params
     * _id {string}
    */ 
    activate:async(req,res,next) => {
        try {
            const reg = await models.Device.findOneAndUpdate({_id:req.body._id},{deleted_at:null},{useFindAndModify: false});
            res.status(200).json(reg);
        } catch (e) {
            res.status(500).send({
                message: 'Ocurrió un error' 
            });
            next(e);
        }
    },
    /**
     * Deactivate an specific Device
     * @params
     * _id {string}
    */ 
    deactivate:async(req,res,next) => {
        try {
            const reg = await models.Device.findByIdAndUpdate({_id:req.body._id},{deleted_at:Date.now()},{useFindAndModify: false});
            res.status(200).json(reg);
        } catch (e) {
            res.status(500).send({
                message: 'Ocurrió un error' 
            });
            next(e);
        }
    },
    /**
     * Assign device to user
     * @body
     * name {string}
     * last_name {string}         
     * email {string}
     * {password}
    */ 
    assign: async(req,res,next) => {
      try {
          const user = await token.decode(req.headers.token);
          let data = {
            name: req.body.name,
            description: req.body.description,
            user: user._id
          }
          const device = await models.Device.findOne({_id:req.body._id});
          let match = await brcypt.compare(req.body.password,device.password);
          if(match){
            const reg = await models.Device.findOneAndUpdate({_id:device._id},{ $set: data },{useFindAndModify: false});
            res.status(200).json(reg);
          }
          else{
            res.status(404).send({
              message: 'Password incorrecto'
            });
          }
      } catch (e) {
          res.status(500).send({
              message: 'Ocurrió un error' 
          });
          next(e);
      }
    }
}