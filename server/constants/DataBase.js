import models from '../models';
import brcypt from 'bcryptjs';

/**
 * Provide const data settings default
 * 
 * @return object
 */
export default {
  /** Create default user */
  defaultUser: async() => {
      try {
        /** Create default user */
        if(!await models.User.findOne({email:"danrdzprz@carcosa.com"})){
            const password = await brcypt.hash("123",10);
            await models.User.create({
                username:"danrdzprz",
                name:"Daniel",
                last_name:"Rodríguez",
                email:"danrdzprz@carcosa.com",
                password:password,
            });
        }
    } catch (e) {
        console.log('User default not exist', e);
    }
  },
  /** Zima Manager info */
  device: async() => {
    try {
      if(!await models.Device.findOne({name:"danrdzprz"})){
        const user = await models.User.findOne({email:"danrdzprz@carcosa.com"});
        if(user){
          const password = await brcypt.hash("123",10);
          await models.Device.create({
              name: "danrdzprz",
              password: password,
              // mac: "F0:E1:D2:C3:B4:A5",
              user: user._id,
              description: "Dispositivo de Daniel"
            });
          }
        }
    } catch (e) {
      console.log("Defualt devies can't not be created", e);
    }
  }
};