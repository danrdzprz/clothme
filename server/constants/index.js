import DataBase from "./DataBase";
import Logger from "../providers/logger";

/**
 * Provides all constants
 * 
 * @return object
 */
export default {
  load: async()=>{
    await DataBase.defaultUser();
    await DataBase.device();
    Logger.info('⏱ Default Data created!');
  }
};