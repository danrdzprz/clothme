import express from 'express';
import config from './config';
import Logger from './providers/logger';
import socketIo from "socket.io";
/**
 * Start server
 */
async function startServer() {
    const app = express();
    const io = socketIo();
    /**
     * A little hack here
     * Import/Export can only be used in 'top-level code'
     * Well, at least in node 10 without babel and at the time of writing
     * So we are using good old require.
     */
    await require('./providers').default({ expressApp: app });
  
    const server = app.listen(config.port,config.host, err => {
      if (err) {
        Logger.error(err);
        process.exit(1);
        return;
      }
      Logger.info(`
        ################################################
        🛡️  Server listening on : ${config.host} : ${config.port} 🛡️ 
        ################################################
      `);
    });

    io.listen(server);
    require('./providers/socket').default({ io: io })

    app.set('io', io);

}

startServer()
.then((r) => {
  Logger.info(`👀 😜 System mode: ${config.mode}`);
})
.catch(e => console.error(`.catch(${e})`));