import dotenv from 'dotenv';

process.env.TZ = 'America/Mexico_City'; 
// Set the NODE_ENV to 'development' by default
process.env.APP_ENV = process.env.APP_ENV || 'development';

const envFound = dotenv.config({ path: __dirname+'/../.env' });
if (!envFound) {
  // This error should crash whole process
  throw new Error("⚠️  Couldn't find .env file  ⚠️");
}

export default {
  /**
   * System mode
   */
  mode : process.env.APP_ENV,

  /**
   * Your port
   */
  port: process.env.PORT || 3000,

  /**
   * Your host
   */
  host: process.env.APP_HOST || 'localhost',

  /** 
    * Database credentials
  */
  database :{
    port: process.env.DB_PORT,
    host: process.env.DB_HOST,
    database: process.env.DB_DATABASE,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD
  },

  /**
   * Redis credentials
   */
  redis:{
    host:process.env.REDIS_HOST,
    port:process.env.REDIS_PORT,
    password:process.env.REDIS_PASSWORD,
  },
  /**
   * That long string from mlab
   */
  databaseURL: process.env.MONGODB_URI,

  /**
   * Your secret sauce
   */
  jwtSecret: process.env.JWT_SECRET,

  /**
   * Used by winston logger
   */
  logs: {
    level: process.env.LOG_LEVEL || 'silly',
  },

  /**
   * Agenda.js stuff
   */
  agenda: {
    dbCollection: process.env.AGENDA_DB_COLLECTION,
    pooltime: process.env.AGENDA_POOL_TIME,
    concurrency: parseInt(process.env.AGENDA_CONCURRENCY, 10),
  },

  /**
   * Agendash config
   */
  agendash: {
    user: 'agendash',
    password: '123456'
  },

  /**
   * API configs
   */
  api: {
    prefix: '/api',
  },
  
  /**
   * Mailgun email credentials
   */
  emails: {
    apiKey: 'API key from mailgun',
    domain: 'Domain Name from mailgun'
  }
};