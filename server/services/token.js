import jwt from 'jsonwebtoken';
import models from '../models';
import config from '../config';
// import redis_client from '../providers/redis';
// import { promisify } from "util";

// const getAsync = promisify(redis_client.SISMEMBER).bind(redis_client);
/**
 * Verify if a token belongs to a valid user
 * 
 * @param {*} token 
 * 
 * @return boolean
 */
async function checkToken(token){
    let __id = null;
    try {
        const{_id} = await jwt.decode(token);
        __id = _id;
    } catch (e) {
        return false;
    }
    const user = await models.User.findOne({_id:__id,deleted_at:null});
    if(user){
        // const token = jwt.sign({_id:user._id},config.jwtSecret,{expiresIn:'1d'});
        // return {token};
        const token_blacklist = await models.Blacklist.create({token:token});
        const reg = await models.Session.findOneAndDelete({user:user._id,token:token},{useFindAndModify: false});
        return false;
    }else{
        return false;
    }
}
export default {
    /** generate a signed json web token and return it in the response 
     * @params 
     *          _id {string}
     *          email {email}
     * @return token {string}
    */
    encode: async (_id,email) => {
        const token = jwt.sign({_id:_id,email:email},config.jwtSecret,{expiresIn:'1d'});
        return token;
    },
    /** decode a json web token that not be in blacklist, get the payload and verify if belongs to valid user
     * @params 
     *          token {string}
     * @return  boolean|string
    */
    decode: async(token)=>{
        try {
            const token_blacklist = await models.Blacklist.findOne({token:token});
            // if(await getAsync('token_blacklist',token)){
            if(token_blacklist){
                return false;
            }
            else{
                const {_id} = await jwt.verify(token,config.jwtSecret);
                const user = await models.User.findOne({_id:_id,deleted_at:null}); 
                if(user){
                    return user;
                }else{
                    return false;
                }
            }
        } catch (e) {
            const newToken = await checkToken(token);
            return newToken;
        }
    }
}