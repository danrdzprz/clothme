import redis from 'redis';
import config from '../config';
import Logger from './logger';

/**
 * Provide conection to redis
 * 
 * @return object
 */
const options = {
    "host": config.redis.host,
    "port": config.redis.port,
    "password": config.redis.password
};
const redisClient = redis.createClient(options)
.on('connect',()=>{
    Logger.info('🚀🚀 Redis loaded and connected! 🚀🚀')
})
.on('error', (error)=>{
    console.log('Redis not connected', error)
});


export default redisClient;
