import express from 'express'; // framework
import morgan from 'morgan'; //middleware : funciona para tener un registro de acceso a la aplicacion
import cors from 'cors'; //middleware : para realizar peticiones remotas a nuestro servidor
import path from 'path'; 
import router from '../routes'; // routes
import config from '../config'; //config
//middleware son funciones que se ejecutan antes de entrar en la ruta
/**
 * Inject dependecies
 * 
 *  @return object
 */
export default ({ app }) => {
    app.use(morgan('dev')); 
    app.use(cors());
    app.use(express.json());
    app.use(express.urlencoded({extended:true}));
    app.use(express.static(path.join(__dirname,'../public')));//archivos estaticos que seran publicos
    app.set('port',config.port || 3000 );
    app.use('/api',router);
}