import mongoose from 'mongoose';
import config from '../config';

/**
 * Provide conection to mongo database
 * 
 * @return object
 */
export default async () => {
    // mongoose.connect('mongodb://username:password@host:port/database?options...', {useNewUrlParser: true});
    const dbUrl = `mongodb+srv://${config.database.username}:${config.database.password}@${config.database.host}/${config.database.database}?retryWrites=true&w=majority`;
    let db = null ;
    const connection = await mongoose.connect(dbUrl, {useCreateIndex:true,useNewUrlParser: true,useUnifiedTopology: true })
    .then(mongoose => {
        db = mongoose.connection.db;
    })
    .catch(err => {
        db = null;
        console.log(err);
    });
    return db;
};