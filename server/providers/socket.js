import tokenService from "../services/token";

export default ({ io }) => {
  var rooms = {};

  // middleware
  io.use( async (socket, next) => {
    let token = socket.handshake.query.token;
    const validate = await tokenService.decode(token);
    if(validate){
      return next();
    }else{
      socket.disconnect();
      return next(new Error('authentication error'));
    }
  });

  io.on('connection',(socket) =>{
    console.log(socket.id);
    console.log('A client connected :' + new Date);
    socket.on('join', (data) =>{
        socket.join(data);
        console.log('user join to channel :', data);
    })
    socket.on('leave', (data) =>{
        socket.leave(data);
        console.log('user leave channel : ', data);
    })
    socket.on('disconnect',(data)=>{
        console.log('user disconnected : ' + new Date);
    });
  });
}