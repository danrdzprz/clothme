import expressProvider from './express';
import mongooseProvider from './mongoose';
import Logger from './logger';
import constants from "../constants";


/**
 * Load main services
 * 
 * @return null
 */
export default async ({expressApp}) => {

    const mongoose = await mongooseProvider()
    .then(r => (Logger.info('🚀🚀 DB loaded and connected! 🚀🚀')))
    .catch(e => console.error(`.catch(${e})`));

    await constants.load();

    await expressProvider({ app: expressApp });

    Logger.info('🚀🚀 Express loaded 🚀🚀');
}