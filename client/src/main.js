/*!

=========================================================
* Vue Argon Dashboard - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './registerServiceWorker'
import ArgonDashboard from './plugins/argon-dashboard'
import Vuex from 'vuex'
import { store } from './store'
import { api_url } from './constants/config'
import axios from 'axios'
// import VueGoodTablePlugin from 'vue-good-table';
import  VueGoodTablePlugin  from "vue-good-table/src";

// import the styles 
import 'vue-good-table/dist/vue-good-table.css'

axios.defaults.baseURL = api_url;

Vue.use(Vuex);

Vue.config.productionTip = false

Vue.use(ArgonDashboard)
// new Vue({
  //   store,
  //   router,
  //   render: h => h(App)
  // }).$mount('#app')
  
Vue.use(VueGoodTablePlugin);

export default new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})

