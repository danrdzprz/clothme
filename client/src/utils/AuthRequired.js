import {store} from "../store";

export default async (to, from, next) => {
  await store.dispatch("autoLogin");
  if (localStorage.getItem('token') != null && localStorage.getItem('token').length > 0) {
    // verify with firebase or jwt
    next()
  } else {
    localStorage.removeItem('token')
    next('/login')
  }
}
