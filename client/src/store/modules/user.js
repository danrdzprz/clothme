import axios from "axios";
import decode from "jwt-decode";
import router from "../../router";
import SocketIO from "socket.io-client";
import Vue from "vue";
import GoogleMapsApiLoader from 'google-maps-api-loader'; 
import {google_api_key} from "../../constants/config";
import { mapState } from 'vuex';

//socket.io
Vue.prototype.$io = {};
Vue.prototype.$googleMap = {};

function socketCreate(token) {
  const io = SocketIO(process.env.VUE_APP_ROOT_WEBSOCKET, {
    autoConnect: false,
    query: "token=" + token
  });
  io.open();
  Vue.prototype.$io = io;
}
//gmap
async function gMap(){
  const googleMapApi = await GoogleMapsApiLoader({
    libraries: ["places","drawing"],
    apiKey: google_api_key
  });
  Vue.prototype.$googleMap=googleMapApi;
}

export default {
  state: {
    // currentUser: localStorage.getItem('user') != null ? JSON.parse(localStorage.getItem('user')) : null,
    currentUser: null,
    loginError: null,
    processing: false
  },
  getters: {
    currentUser: state => state.currentUser,
    processing: state => state.processing,
    loginError: state => state.loginError
  },
  mutations: {
    setUser (state, payload) {
      state.currentUser = payload
      state.processing = false
      state.loginError = null
    },
    setLogout (state) {
      state.currentUser = null
      state.processing = false
      state.loginError = null
    },
    setProcessing (state, payload) {
      state.processing = payload
      state.loginError = null
    },
    setError (state, payload) {
      state.loginError = payload
      state.currentUser = null
      state.processing = false
    },
    clearError (state) {
      state.loginError = null
    }
  },
  actions: {
    async login ({ commit }, payload) {
      commit('clearError')
      commit('setProcessing', true)
      return axios
        .post("user/login", {email:payload.email,password:payload.password})
        .then(response => {
          return response.data;
        })
        .then(async data =>  {
          const item = { 
            uid: data.user._id, 
            title: data.user.name,
            img: '/assets/img/profile-pic-l.jpg',
            date: 'Last seen today 15:24' 
          };
          // localStorage.setItem('user', JSON.stringify(item));
          localStorage.setItem("token", data.tokenReturn);
          axios.defaults.headers.common["Token"] = data.tokenReturn;
          commit('setUser', item);
          router.push("/");
          socketCreate(data.tokenReturn);
          await gMap();
        })
        .catch(error => {
          if (error.response) {
            if (error.response.data) {
              if (error.response.data.errors) {
                Object.entries(error.response.data.errors).forEach(([key, msj]) => {
                    commit('setError', msj);
                  }
                );
              }
              if (error.response.data.message) {
                commit('setError', error.response.data.message);
              }
            }
          }
        });
    },
    async autoLogin  ({ commit }){
      commit('clearError');
      let token = localStorage.getItem("token");
      if (token) {
        axios.defaults.headers.common["Token"] = token;
        return axios
          .get("user/me")
          .then(response => {
            return response.data;
          })
          .then(async data => {
            const item = { 
              uid: data.response._id, 
              title: data.response.name,
              img: '/assets/img/profile-pic-l.jpg',
              date: 'Last seen today 15:24' 
            };
            commit('setUser', item);
            socketCreate(token);
            await gMap();
          })
          .catch(error => {
            if (error.response) {
              if (error.response.data) {
                if (error.response.data.errors) {
                  Object.entries(error.response.data.errors).forEach(([key, msj]) => {
                      commit('setError', msj);
                    }
                  );
                }
                if (error.response.data.message) {
                  commit('setError', error.response.data.message);
                  localStorage.removeItem("user");
                  localStorage.removeItem("token");
                  router.push('/user/login').then(()=>{
                    commit('setLogout');
                  });
                }
              }
            }
          });
      }
      return null 
    },
    async signOut ({ commit }) {
      commit('clearError');
      return axios
        .post("user/logout")
        .then(response => {
          localStorage.removeItem("user");
          localStorage.removeItem("token");
          router.push('/login').then(()=>{
            commit('setLogout');
          });
        })
        .catch(error => {
          if (error.response) {
            if (error.response.data) {
              if (error.response.data.errors) {
                Object.entries(error.response.data.errors).forEach(([key, msj]) => {
                    commit('setError', msj);
                  }
                );
              }
              if (error.response.data.message) {
                commit('setError', error.response.data.message);
              }
            }
          }
        });
    }
  }
}
