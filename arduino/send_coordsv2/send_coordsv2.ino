/*
### Connect TCP and send GET request.
1. This example is used to test DFRobot_SIM808 GPS/GPRS/GSM Shield's connect TCP and send GET request.
2. Open the SIM808_TCPConnection example or copy these code to your project
3. Download and dial the function switch to Arduino
4. Open serial helper
5. Waiting for a few minutes, until serial has sent "Connect mbed.org success"
6. Serial will send "Hello world!"
create on 2016/09/23, version: 1.0
by jason
*/
#include <DFRobot_sim808.h>
#include <SoftwareSerial.h>

#define PIN_TX    10
#define PIN_RX    11
SoftwareSerial mySerial(PIN_TX,PIN_RX);
DFRobot_SIM808 sim808(&mySerial);//Connect RX,TX,PWR,

//make sure that the baud rate of SIM900 is 9600!
//you can use the AT Command(AT+IPR=9600) to set it through SerialDebug

//DFRobot_SIM808 sim808(&Serial);
char _host[] = "375230c2.ngrok.io";
String method = "GET";
String host = "375230c2.ngrok.io";
String route="/api/coordinates/test";
String device = "5de71f025e3dc452483fe5c0";

char latitude[20];
char longitude[20];

double lat = 22.177553511569663;
double lng = -101.0103077670438;
//variables para controlara cada cuando se van a mandar las coordenadas
#define INTERVAL_REQUEST 5000
unsigned long time_request = 0;
bool make_request = true;
//++++++++++++++++++++++
int messageIndex = 0;
char buffer[512];

void setup(){
  mySerial.begin(9600);
  Serial.begin(9600);
  
  //******** Initialize sim808 module *************
  while(!sim808.init()) {
      delay(1000);
      Serial.print("Sim808 init error\r\n");
  }
  delay(3000);  

 //************* Turn on the GPS power************
  while(!sim808.attachGPS()){
    Serial.println("Open the GPS power failure");
    delay(2000);
  }
  Serial.println("Open the GPS power success");

  //*********** Attempt DHCP APN*******************
  while(!sim808.join(F("internet.itelcel.com"),F("webgprs"),F("webgprs2002"))) {
  //while(!sim808.join(F("cmnet"))) {
      Serial.println("Sim808 join network error");
      delay(2000);
  }
  //************ Successful DHCP ****************
  Serial.print("IP Address is ");
  Serial.println(sim808.getIPAddress());

  //*********** Establish a TCP connection ************
  while(!sim808.connect(TCP,_host, 80)) {
  }
  Serial.print("Connect success: ");
  Serial.println(host);
  /*if(!sim808.connect(TCP,_host, 80)) {
      Serial.println("Connect error");
  }else{
    Serial.print("Connect success: ");
    Serial.println(host);
  }*/
   //make_request = false;
   //Serial.println("Init Success, please send SMS message to me!");
}

void loop(){
  /*messageIndex = sim808.isSMSunread();
  if(messageIndex > 0){
    make_request = true;
  }*/
  while ( make_request){
    make_request = false;//vamos a esperar a que se concrete la peticion
    getGPSData();
  }
}

void sendRequest(){

  //*********** Attempt DHCP APN*******************
  /*while(!sim808.join(F("internet.itelcel.com"),F("webgprs"),F("webgprs2002"))) {
  //while(!sim808.join(F("cmnet"))) {
      Serial.println("Sim808 join network error");
      delay(2000);
  }
  //************ Successful DHCP ****************
  Serial.print("IP Address is ");
  Serial.println(sim808.getIPAddress());

  //*********** Establish a TCP connection ************
  if(!sim808.connect(TCP,_host, 80)) {
      Serial.println("Connect error");
  }else{
      Serial.print("Connect success: ");
      Serial.println(host);
  }*/
  //*********** Send a GET request *****************
  Serial.println("waiting to fetch...");

  //Convertimos las coordenadas obtenidas por el gps en caracteres y mandamos a un arreglo de caracteres
  dtostrf(lat, 4, 6, latitude); //put float value of la into char array of lat. 4 = number of digits before decimal sign. 6 = number of digits after the decimal sign.
  dtostrf(lng, 4, 6, longitude); //put float value of lo into char array of lng

  //concatenamos todas las variables para obtener la peticion que se va hacer
  String greeting = method + " "+ route +"?device="+device+"&latitude="+latitude+"&longitude="+longitude+" HTTP/1.1\r\nHost: "+host+"\r\n\r\n";
  //despues pasamos el string a una cadena de caracteres, solo asi podremos hacer la peticion
  int str_len = greeting.length() + 1; 
  char http_cmd[str_len];
  greeting.toCharArray(http_cmd, str_len);
  
  Serial.println(http_cmd);
  //Serial.println(sizeof(http_cmd)-1);
  sim808.send(http_cmd, sizeof(http_cmd)-1);
  while (true) {
      // peermite volver a hacer una peticion
      int ret = sim808.recv(buffer, sizeof(buffer)-1);
      if (ret <= 0){
          Serial.println("fetch over...");
          break; 
      }
      buffer[ret] = '\0';
      Serial.print("Recv: ");
      Serial.print(ret);
      Serial.print(" bytes: ");
      Serial.println(buffer);
      break;
  }
  //************* Close TCP or UDP connections **********
  //sim808.close();

  //*** Disconnect wireless connection, Close Moving Scene *******
  //sim808.disconnect();
  delay(2000);
  make_request = true;
}

void getGPSData(){
  //************* Turn on the GPS power************
  /*while(!sim808.attachGPS()){
    Serial.println("Open the GPS power failure");
    delay(2000);
  }
  Serial.println("Open the GPS power success");*/
  Serial.println("please, wait for it!");
  while(!sim808.getGPS()){
  }
  Serial.println("Now you Can get gps data");
  
  Serial.print("latitude :");
  Serial.println(sim808.GPSdata.lat,6);
  lat = sim808.GPSdata.lat;
  Serial.print("longitude :");
  Serial.println(sim808.GPSdata.lon,6);
  lng = sim808.GPSdata.lon;
  //************* Turn off the GPS power ************
 //sim808.detachGPS();
  sendRequest();
  
  
}
